from __future__ import print_function
import sys
import requests

ip = sys.argv[1]

url = "https://www.threatstop.com/api/public/check/ip"
headers = {"X-OEM-Key":"E4C2-7846-4604-60EF"}
res = requests.put(
    url, json={"ip":ip}, headers=headers
)
print(res)
print(res.content)