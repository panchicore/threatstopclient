# coding=utf-8
import json
import logging

from .exceptions import (BadResponse, DeleteError, ThreadStopException, HTTPBadRequest,
                               HTTPUnauthorized, HTTPForbidden,
                               HTTPServerError, HTTPConflict, HTTPNotFound,
                               HTTPTooManyRequests)
import requests

class ThreadStopRestClient(object):

    def __init__(self, api_key=None):

        # Require api key credentials.
        if not api_key:
            raise ThreadStopException("API KEY required into the ThreadStopRestClient constructor")

        self.session = requests.Session()
        self.headers = {
            'Content-Type': 'application/json',
            'X-OEM-Key': api_key
        }

    def _request(self, method, url, **kwargs):
        """
        A simple wrapper around requests.
        """
        return self.session.request(method, url, **kwargs)

    def make_request(self, url, data={}, method=None, **kwargs):
        """
        Builds and makes the HTTP Basic Request, catches errors
        """
        if not method:
            method = 'POST' if data else 'GET'

        try:
            response = self._request(method, url,
                                     data=data,
                                     headers=self.headers,
                                     **kwargs)
        except Exception:
            raise

        if response.status_code == 401:
            raise HTTPUnauthorized(response.content)
        elif response.status_code == 403:
            raise HTTPForbidden(response.content)
        elif response.status_code == 404:
            raise HTTPNotFound(response.content)
        elif response.status_code == 409:
            raise HTTPConflict(response)
        elif response.status_code == 429:
            exc = HTTPTooManyRequests(response)
            exc.retry_after_secs = int(response.headers['Retry-After'])
            raise exc

        elif response.status_code >= 500:
            raise HTTPServerError(response.content)
        elif response.status_code >= 400:
            raise HTTPBadRequest(response.content)
        return response


class ThreadStop(object):
    API_ENDPOINT = "https://www.threatstop.com/api/public"
    API_VERSION = 1
    logger = logging.getLogger("threadstop")

    def __init__(self, api_key=None):
        self.client = ThreadStopRestClient(api_key)

    def make_request(self, *args, **kwargs):
        # This should handle data level errors, improper requests, and bad
        # serialization

        method = kwargs.get('method', 'POST' if 'data' in kwargs else 'GET')
        response = self.client.make_request(*args, **kwargs)

        if response.status_code == 202:
            return True
        if method == 'DELETE':
            if response.status_code == 204:
                return True
            else:
                raise DeleteError(response)
        try:
            resp = json.loads(response.content.decode('utf8'))
        except ValueError:
            raise BadResponse

        return resp

    def check_ip(self, ip, last_seen=None, targets=None, strategy=None):
        """

            Use this method to check what we know about an array of IPv4 addresses.
            https://www.threatstop.com/api/public/
            doc: http://d.pr/i/1lG5n

        :param ip: list of IPs to be checked.
        :return: The API will return a response in the JSON format.
        If the syntax was correct the “result” field value would be
        “OK” if not it will be “Error” and an appropriate message is given.
        """
        url = "{0}/check/ip".format(self.API_ENDPOINT)
        data = {"ip": self._csv_values_to_list(ip)}
        logging.info("{0} data={1}".format(url, data))
        return self.make_request(url, json=data, method="PUT")

    def _csv_values_to_list(self, values):
        """
        Convert string to lists when the API requires.
        :param values:
        :return:
        """
        if type(values) is list:
            return values

        result = []
        for v in values.split(","):
            if v:
                result.append(v.strip())
        return result

