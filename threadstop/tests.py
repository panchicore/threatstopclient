from django.test import TestCase
from threadstop.api import ThreadStop
from threadstop.exceptions import HTTPForbidden
from threadstopclient.settings import THREADSTOP_API_KEY


class APITest(TestCase):

    api_key = "INVALID"

    def test_forbidden_request(self):
        try:
            t = ThreadStop(self.api_key)
            res = t.check_ip("123.123.123.123")
        except Exception as e:
            self.assertEqual(e.__class__, HTTPForbidden)

    def test_check_ip_request(self):
        self.api_key = THREADSTOP_API_KEY
        t = ThreadStop(self.api_key)
        res = t.check_ip("123.123.123.123")
        self.assertEqual(res["result"], "OK")
