from django.forms import forms

class CheckIPUploadCSVFile(forms.Form):
    file = forms.FileField(
        label="Or upload as CSV file",
        required=False
    )