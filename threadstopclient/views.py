import json

from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect

from threadstop.api import ThreadStop
from threadstopclient import settings
from threadstopclient.forms import CheckIPUploadCSVFile


def home(request):
    return redirect(
        reverse("check-ip")
    )


def check_ip(request):

    check = None
    if request.method == "POST":
        api = ThreadStop(settings.THREADSTOP_API_KEY)
        form = CheckIPUploadCSVFile(request.POST, request.FILES)
        if form.is_valid():
            if len(form.files) > 0:
                file = request.FILES['file']
                ips = file.read()
                ips = ips.replace("\n", ",")
                ips = ips.replace(" ", ",")
            else:
                ips = request.POST.get("ip")
            check = api.check_ip(ips)

    else:
        form = CheckIPUploadCSVFile()

    data = {
        "form": form,
        "check": check
    }

    return render(request, "check_ip.html", data)